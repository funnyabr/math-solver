# main.py

# imports
import os
import tkinter
from tkinter import *
from tkinter import filedialog
from PIL import ImageGrab, ImageTk, Image

# the prediction function that uses the trained model to predict
# the hand written math problem given as an image
from test import predict_img
from check import cut_img

# Just disables the warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# mouse location and left button status
b1 = "up"
x_old, y_old = None, None
count = 0


def b1down(event):
    # called when left mouse button is pressed
    global b1
    b1 = "down"


def b1up(event):
    # called when left mouse button released
    global b1, x_old, y_old
    b1 = "up"
    x_old = None
    y_old = None


def motion(event):
    # called when the mouse is moving
    if b1 == "down":
        # if left mouse button pressed
        global x_old, y_old
        if x_old is not None and y_old is not None:
            event.widget.create_line(x_old, y_old, event.x, event.y, smooth=TRUE, width=4)
        x_old = event.x
        y_old = event.y


def recognition(window, str_math, img_name):
    for widget in window.winfo_children():
        # delete the canvas and the buttons - moves to the solve screen
        widget.destroy()

    try:
        # try to solve the math problem
        eval(str_math)
    except SyntaxError:
        # if not recognized well or not written well
        tkinter.Label(window, text="The math problem is not valid", font=("papyrus", 30)).pack()
    else:
        # print the recognized math problem and the solution
        tkinter.Label(window, text="The math problem is:   " + str_math, font=("papyrus", 30)).pack()
        tkinter.Label(window, text="And the answer is:   " + str(eval(str_math)), font=("papyrus", 30)).pack()
        tkinter.Label(window, text="Did I recognized correctly?", font=("papyrus", 20)).place(y=200)
        button_yes = Button(window, fg="green", text="YES", command=lambda: recognized())
        button_yes.place(x=100, y=270, width=70, height=25)
        button_no = Button(window, fg="green", text="NO", command=lambda: not_recognized())
        button_no.place(x=200, y=270, width=70, height=25)

        # save chars in the correct directory of the data set
        def add_chars():
            window.lift()
            im_name = str(count) + ".jpg"
            img = Image.open(im_name)
            img2 = ImageTk.PhotoImage(img)
            panel = Label(window, image=img2)
            panel.img = img2
            panel.place(x=280, y=200)

        def add_to_data():
            global count
            # open file dialog and choose the right file to save the drawing
            file = filedialog.asksaveasfile(mode='w', defaultextension=".jpg")
            if file:
                # saves the image to the input file name.
                im_name = str(count) + ".jpg"
                img = Image.open(im_name)
                img.save(file)
                count -= 1
                if count:
                    add_chars()
                else:
                    for widget2 in window.winfo_children():
                        widget2.destroy()
                    window.lift()
                    tkinter.Label(window, text="Saved Successfully!", font=("papyrus", 20)).pack()

        # if not recognized
        def not_recognized():
            for widget2 in window.winfo_children():
                widget2.destroy()
            tkinter.Label(window, text="Sorry I couldn't recognize your hand writing, \n Saving the characters "
                                       "to the data set!", font=("papyrus", 20)).pack()

            button_add = Button(window, fg="green", text="add", command=lambda: add_to_data())
            button_add.place(x=350, y=220, width=70, height=25)
            global count
            count = cut_img(img_name) - 1
            add_chars()

        def recognized():
            for widget2 in window.winfo_children():
                widget2.destroy()
            tkinter.Label(window, text="YAY! \n WE DID IT!", font=("papyrus", 20)).pack()


def write():
    """runs when write button pressed"""

    # window setting
    window2 = Toplevel()
    window2.geometry("600x330")
    drawing_area = Canvas(window2, width=600, height=300, background='white')
    drawing_area.pack()

    # if left mouse button is pressed and the mouse is moving, write on the screen
    drawing_area.bind("<Motion>", motion)
    drawing_area.bind("<ButtonPress-1>", b1down)
    drawing_area.bind("<ButtonRelease-1>", b1up)

    # buttons and label settings
    button_solve = Button(window2, fg="green", text="Solve", command=lambda: solve(drawing_area))
    button_solve.pack(side=RIGHT)
    button_clear = Button(window2, fg="green", text="Clear", command=lambda: delete(drawing_area))
    button_clear.pack(side=LEFT)
    message = Label(window2, text="Press and Drag to write")
    message.pack(side=BOTTOM)

    def delete(widget):
        """input: the canvas widget
        the function clears the canvas when Clear button is pressed"""
        widget.delete("all")

    def solve(widget):
        """input: the canvas widget
        the function saves the window the drawing area as an image"""
        x = window2.winfo_rootx() + widget.winfo_x()
        y = window2.winfo_rooty() + widget.winfo_y()
        x1 = x + widget.winfo_width()
        y1 = y + widget.winfo_height()
        im = ImageGrab.grab().crop((x, y, x1, y1))
        im.save("drawing.jpg")

        # predict the hand written math problem to a string
        s = predict_img("drawing.jpg")
        recognition(window2, s, "drawing.jpg")


def file_dialog():
    """runs when upload button pressed"""

    # opens file dialog to choose an image of math problem
    file_name = ""
    file_name = filedialog.askopenfilename(initialdir="/", title="Select an image file",
                                           filetype=(("jpeg", "*.jpg"), ("All Files", "*.*")))
    if file_name != "":
        # if file was chosen

        # window settings
        window3 = Toplevel()
        window3.geometry("600x330")

        # load image and resize to fit the window
        img = Image.open(file_name)
        im_resize = img.resize((480, 220))
        img = ImageTk.PhotoImage(im_resize)

        # The Label widget is a standard Tkinter widget used to display a text or image on the screen.
        # show the image
        panel = Label(window3, image=img)
        panel.img = img

        # The Pack geometry manager packs widgets in rows or columns.
        panel.pack()

        # solve button settings
        button_solve = Button(window3, fg="green", text="Solve", command=lambda: solve2())
        button_solve.pack(side=BOTTOM)

    def solve2():
        # runs when solve button pressed after uploading an image
        # predict the hand written image with the model
        s = predict_img(file_name)
        recognition(window3, s, file_name)


def instructions():
    """runs when "?" button pressed"""

    # window settings
    window4 = Toplevel()
    window4.geometry("600x450")

    # show the instruction image
    instructions_img = ImageTk.PhotoImage(Image.open("instructions.jpg"))
    img_label = Label(window4, image=instructions_img)
    img_label.img = instructions_img
    img_label.pack()


def add():
    """runs when add button pressed"""

    # window settings
    window5 = Toplevel()
    window5.geometry("100x100")
    drawing_area = Canvas(window5, width=45, height=45, background='white')
    drawing_area.pack()

    # if left mouse button is pressed and the mouse is moving, write on the screen
    drawing_area.bind("<Motion>", motion)
    drawing_area.bind("<ButtonPress-1>", b1down)
    drawing_area.bind("<ButtonRelease-1>", b1up)

    # buttons and label settings
    button_solve = Button(window5, fg="green", text="Save", command=lambda: save(drawing_area))
    button_solve.pack(side=RIGHT)
    button_clear = Button(window5, fg="green", text="Clear", command=lambda: delete(drawing_area))
    button_clear.pack(side=LEFT)

    def delete(widget):
        """input: the canvas widget
        the function clears the canvas when Clear button is pressed"""
        widget.delete("all")

    def save(widget):
        """input: the canvas widget
        the function saves the drawing area as an image"""
        x = window5.winfo_rootx() + widget.winfo_x() + 4
        y = window5.winfo_rooty() + widget.winfo_y() + 4
        x1 = x + widget.winfo_width() - 4
        y1 = y + widget.winfo_height() - 4
        im = ImageGrab.grab().crop((x, y, x1, y1))

        # open file dialog and choose the right file to save the drawing
        file = filedialog.asksaveasfile(mode='w', defaultextension=".jpg")
        if file:
            # saves the image to the input file name.
            im.save(file)


# set window
root = Tk()
root.title('Math Solver for beginners')
root.geometry("800x500")
Label(root, text="MATH SOLVER", fg='plum1', bg='gray15', font="Verdana 28 bold").pack()
root.configure(background='gray15')

# set buttons for the main screen
button1 = tkinter.Button(root, text='Upload', width=30, bg='plum1', command=file_dialog)
button2 = tkinter.Button(root, text='Write', width=30, bg='plum1', command=write)
button3 = tkinter.Button(root, text='Add', width=30, bg='plum1', command=add)
photo = PhotoImage(file='q_button.png')
button5 = tkinter.Button(root, bd='0', image=photo, command=instructions)
button1.place(x=300, y=150)
button2.place(x=300, y=220)
button3.place(x=300, y=290)
button5.place(x=0, y=0)

# An infinite loop that runs the application,
# waits for an event to occur and process the event as long as the window is not closed
root.mainloop()
