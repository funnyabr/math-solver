# Data_extraction.py

# imports
import numpy as np
import cv2
import os
import pandas as pd


def load_images_from_folder(folder):
    """input: path to a file with images.
    returns list of arrays, each array representing an image from the file"""

    # initialize variables
    train_data = []
    maxi = 0
    x_max = 0
    y_max = 0
    h_max = 0
    w_max = 0

    # goes throw all the images in the given path
    for filename in os.listdir(folder):

        # load an image in grayscale mode
        img = cv2.imread(os.path.join(folder, filename), cv2.IMREAD_GRAYSCALE)

        if img is not None:
            # makes the image total black and white,
            # and invert the colors (black to white and the opposite)
            ret_val, thresh_img = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY_INV)

            # returns a list of arrays (array for each white object)
            # each array contains vectors of points that on the contour
            contours, hierarchy = cv2.findContours(thresh_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

            # sort the contours list (the arrays from left object to right)
            # each array from the upper left point as opposed to clockwise
            sorted_cnt = sorted(contours, key=lambda ctr: cv2.boundingRect(ctr)[0])

            # for each array (object in the img) in the sorted list,
            # find the biggest object (rect)
            for c in sorted_cnt:
                x, y, w, h = cv2.boundingRect(c)
                maxi = max(w * h, maxi)
                if maxi == w * h:
                    x_max = x
                    y_max = y
                    w_max = w
                    h_max = h

            # crop img
            im_crop = thresh_img[y_max:y_max + h_max + 10, x_max:x_max + w_max + 10]
            # resize to 28x28
            im_resize = cv2.resize(im_crop, (28, 28))
            # normalize pixels
            im_resize = im_resize / 255

            # turns the image to numpy array of pixel values
            im_resize = np.reshape(im_resize, (784, 1))
            train_data.append(im_resize)

    # returns list of numpy arrays each contain pixel values of each image
    return train_data


# last value of each array that represent an image will be the title
# assign '-' the title '10'
data = load_images_from_folder('-')
for i in range(0, len(data)):
    data[i] = np.append(data[i], ['10'])
data_test = data[0:100]
data = data[100:]
print(len(data))
print(len(data_test))

# assign '+' the title '11'
data11 = load_images_from_folder('+')
for i in range(0, len(data11)):
    data11[i] = np.append(data11[i], ['11'])
data_test = np.concatenate((data_test, data11[0:100]))
data = np.concatenate((data, data11[100:]))
print(len(data))
print(len(data_test))

# assign * the title '12'
data12 = load_images_from_folder('times')
for i in range(0, len(data12)):
    data12[i] = np.append(data12[i], ['12'])
data_test = np.concatenate((data_test, data12[0:100]))
data = np.concatenate((data, data12[100:]))
print(len(data))
print(len(data_test))

data0 = load_images_from_folder('0')
for i in range(0, len(data0)):
    data0[i] = np.append(data0[i], ['0'])
data_test = np.concatenate((data_test, data0[0:100]))
data = np.concatenate((data, data0[100:]))
print(len(data))
print(len(data_test))

data1 = load_images_from_folder('1')
for i in range(0, len(data1)):
    data1[i] = np.append(data1[i], ['1'])
data_test = np.concatenate((data_test, data1[0:100]))
data = np.concatenate((data, data1[100:]))
print(len(data))
print(len(data_test))

data2 = load_images_from_folder('2')
for i in range(0, len(data2)):
    data2[i] = np.append(data2[i], ['2'])
data_test = np.concatenate((data_test, data2[0:100]))
data = np.concatenate((data, data2[100:]))
print(len(data))
print(len(data_test))

data3 = load_images_from_folder('3')
for i in range(0, len(data3)):
    data3[i] = np.append(data3[i], ['3'])
data_test = np.concatenate((data_test, data3[0:100]))
data = np.concatenate((data, data3[100:]))
print(len(data))
print(len(data_test))

data4 = load_images_from_folder('4')
for i in range(0, len(data4)):
    data4[i] = np.append(data4[i], ['4'])
data_test = np.concatenate((data_test, data4[0:100]))
data = np.concatenate((data, data4[100:]))
print(len(data))
print(len(data_test))

data5 = load_images_from_folder('5')
for i in range(0, len(data5)):
    data5[i] = np.append(data5[i], ['5'])
data_test = np.concatenate((data_test, data5[0:100]))
data = np.concatenate((data, data5[100:]))
print(len(data))
print(len(data_test))

data6 = load_images_from_folder('6')
for i in range(0, len(data6)):
    data6[i] = np.append(data6[i], ['6'])
data_test = np.concatenate((data_test, data6[0:100]))
data = np.concatenate((data, data6[100:]))
print(len(data))
print(len(data_test))

data7 = load_images_from_folder('7')
for i in range(0, len(data7)):
    data7[i] = np.append(data7[i], ['7'])
data_test = np.concatenate((data_test, data7[0:100]))
data = np.concatenate((data, data7[100:]))
print(len(data))
print(len(data_test))

data8 = load_images_from_folder('8')
for i in range(0, len(data8)):
    data8[i] = np.append(data8[i], ['8'])
data_test = np.concatenate((data_test, data8[0:100]))
data = np.concatenate((data, data8[100:]))
print(len(data))
print(len(data_test))

data9 = load_images_from_folder('9')
for i in range(0, len(data9)):
    data9[i] = np.append(data9[i], ['9'])
data_test = np.concatenate((data_test, data9[0:100]))
data = np.concatenate((data, data9[100:]))
print(len(data))
print(len(data_test))

# organize the data list
df = pd.DataFrame(data, index=None)
df_test = pd.DataFrame(data_test, index=None)

# save the data into CSV file
df.to_csv('train_final3.csv', index=False)

# Validation data set is used to test the model during training.
# This is done after some set number of training steps,
# and gives us an indication of how the training is progressing.
# The validation set is used again when training is complete
# to measure the final accuracy of the model
df_test.to_csv('test_final3.csv', index=False)
