# train.py

# imports
import os
import pandas as pd
import tensorflow as tf
from keras.layers import *
from keras.models import Sequential
from keras.utils.np_utils import to_categorical
from keras import backend as k
import matplotlib.pyplot as plt

# Just disables the warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
tf.logging.set_verbosity(tf.logging.ERROR)

# Theano backend
k.set_image_dim_ordering('th')

# read CSV file into DataFrame (first column not used as the index)
df_train = pd.read_csv('train_final3.csv', index_col=False)  # the data set
df_test = pd.read_csv('test_final3.csv', index_col=False)  # the validation set

# the labels for each image in the same order
train_labels = df_train[['784']]
test_labels = df_test[['784']]

# removes the label column from the data frame
df_train.drop(df_train.columns[[784]], axis=1, inplace=True)
df_test.drop(df_test.columns[[784]], axis=1, inplace=True)
train_labels = np.array(train_labels)
test_labels = np.array(test_labels)

# num of classes is the number of object types (0, 1, 2, 3, 4..., 9, '-', '+', '*')
# cat is a numpy array contains a binary matrix representation of each label
cat = to_categorical(train_labels, num_classes=13)
test_cat = to_categorical(test_labels, num_classes=13)

# reshape the data
data_list = []
for i in range(len(df_train)):
    data_list.append(np.array(df_train[i:i + 1]).reshape(1, 28, 28))

test_data_list = []
for i in range(len(df_test)):
    test_data_list.append(np.array(df_test[i:i + 1]).reshape(1, 28, 28))

# Building the neural network
# The number of hidden layers and the number of nodes in them
# were chosen randomly and testing which provide the best accuracy rate.
# activation function(weight1*x1 + weight2*x2...+ bias)
model = Sequential()

# A convolution is the process of applying a filter (“kernel”) to an image
# in a Conv2D layer the values inside the filter matrix are the variables that get tuned
# in order to produce the right output.
# The relu func: return 0 for input<=0, return input for input>0
model.add(Conv2D(30, (5, 5), input_shape=(1, 28, 28), activation='relu'))

# Max pooling is the process of reducing the size of the image
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(15, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

# Dropout is a technique where randomly selected neurons are ignored during training.
# it used for better generalization and to avoid overfitting the training data.
model.add(Dropout(0.2))

# flatten = transform 2D array to a 1D array (vector)
model.add(Flatten())

# dense layer = fully connected
model.add(Dense(128, activation='relu'))
model.add(Dense(50, activation='relu'))

# softmax gets the output array, returns array of probabilities
model.add(Dense(13, activation='softmax'))

# Compile model
# Metrics — Used to monitor the training and testing steps.
# accuracy - the fraction of the images that are correctly classified.
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

# Training the model
# epochs = iterations over the entire data set
# batch_size = number of samples per gradient update
# verbose=1 will show you an animated progress bar
history = model.fit(np.array(data_list), cat, validation_data=(np.array(test_data_list), test_cat),
                    epochs=5, batch_size=32, shuffle=True, verbose=1)

# check the model on the validation data
val_loss, val_acc = model.evaluate(np.array(test_data_list), test_cat)
print(val_loss, val_acc)

# list all data in history
print(history.history.keys())

# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# Save model to disk
# JSON is a simple file format for describing data hierarchically
model_json = model.to_json()
with open("model_final.json", "w") as json_file:
    json_file.write(model_json)

# serialize weights to HDF5
# This is a grid format that is ideal for storing multi-dimensional arrays of numbers
model.save_weights("model_final.h5")
