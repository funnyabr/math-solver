# check.py

# imports
from keras import backend as k
import cv2
import mahotas

# Theano backend
k.set_image_dim_ordering('th')


def cut_img(img_path):
    """input: path to a .jpg file that contains hand written math problem.
    returns the prediction of the problem as a string"""

    # load an image in grayscale mode
    img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)

    if img is not None:
        # makes the image total black and white, and invert the colors (black to white and the opposite)
        ret_val, thresh_img = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY_INV)
        # returns a list of arrays (array for each white object)
        # each array contains vectors of points that on the contour (white object)
        contours, hierarchy = cv2.findContours(thresh_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # sort the contours list (the arrays from left object to right)
        # each array from the upper left point as opposed to clockwise
        cnt = sorted(contours, key=lambda ctr: cv2.boundingRect(ctr)[0])

        rects = []
        for c in cnt:
            # for each array (object in the img) in the sorted list,
            # save the surrounding rectangle in the rects list
            x, y, w, h = cv2.boundingRect(c)
            rect = [x, y, w, h]
            rects.append(rect)

        bool_rect = []
        for r in rects:
            flag_list = []
            for rec in rects:
                flag = 0
                if rec != r:
                    # if there are rectangles that overlap, append flag = 1
                    if r[0] < (rec[0] + rec[2] + 10) and rec[0] < (r[0] + r[2] + 10) and r[1] < (
                            rec[1] + rec[3] + 10) and rec[1] < (r[1] + r[3] + 10):
                        flag = 1
                    flag_list.append(flag)
                if rec == r:
                    flag_list.append(0)
            bool_rect.append(flag_list)

        dump_rect = []
        for i in range(0, len(cnt)):
            for j in range(0, len(cnt)):
                if bool_rect[i][j] == 1:
                    area1 = rects[i][2] * rects[i][3]
                    area2 = rects[j][2] * rects[j][3]
                    if area1 == min(area1, area2):
                        dump_rect.append(rects[i])

        # take the max rectangle from the ones that overlap
        final_rect = [i for i in rects if i not in dump_rect]
        thresh_img = ~thresh_img
        count = 1
        for r in final_rect:
            # crop each object in the image, resize and reshape it
            x = r[0]
            y = r[1]
            w = r[2]
            h = r[3]
            im_crop = thresh_img[y:y + h + 10, x:x + w + 10]
            im_resize = cv2.resize(im_crop, (45, 45))
            # plt.imshow(im_resize, cmap='Greys_r')
            # plt.show()
            mahotas.imsave(str(count) + ".jpg", im_resize)
            count += 1
        return count

